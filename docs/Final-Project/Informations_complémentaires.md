# Les déchets d'impression 3D

L'impression 3D est un outil génial permettant un prototypage rapide et la création d'objets facilement pour toutes et tous chez soi. Malheureusement, comme tout, il y a un revers à la médaille.

La majorité des imprimantes 3D utilisent du plastique, hors [la production de plastique mondiale ne fait qu'augmenter d'année en année](https://plasticoceans.org/the-facts/) et approche les 400 millions de tonne en 2020 . Bien entendu, l'impression 3D n'est pas la seule responsable dans cet océan de plastique, mais représente malgré tout près de [20 000 tonnes de plastique](https://mdpi-res.com/d_attachment/polymers/polymers-13-00744/article_deploy/polymers-13-00744-v2.pdf?version=1614740536).
Les estimations indique que dans ces 20 000 tonnes, 5000 tonnes sont destinés à être des déchets.

Certaines [régions du monde ont une très bonne gestions de leurs déchets plastique](https://www.umsicht.fraunhofer.de/content/dam/umsicht/en/documents/publications/2017/pla-in-the-waste-stream.pdf), mais ce n'est bien entendu pas encore une généralité. Il y a donc une opportunité à dévelloper des méthodes de recyclage à plus petites échèlles et simple à mettre en place à peu près n'importe où.

![](../images/projet/recycling.png)


# [Les propriétés du PLA](https://fr.wikipedia.org/wiki/Acide_polylactique)

* Le PLA (acide polylactique) est un polymère biodégradable, souvent obtenu à base de maïs.
* En dessous de 60°C, le PLA est totalement amorphe.
* A partir de 60°C, le PLA commence à pouvoir être facilement composté.
* A partir de 175°C, le PLA atteint sa temmpérature de fusion, la température qui nous intéresse.


* Tout comme l'acier, le PLA peut adopter différentes structure crystaline en fonction des températures auquelles il refroidi, et donc avoir des propriétés mécanique différentes.
