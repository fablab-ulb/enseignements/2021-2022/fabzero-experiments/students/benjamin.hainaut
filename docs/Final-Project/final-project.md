# Final Project

## La problématique

La première étape est de définir une problèmatique sur laquelle j'aimerais travailler.
Dans un premier temps, j'ai regardé les 17 points d'attention des Nations Unies dans leur [rapport sur le dévellopement durable](https://unstats.un.org/sdgs/report/2021/)

![](../images/projet/17Goals.PNG)

Le point sur lequel j'ai l'impression de pouvoir avoir un poid le plus simplement et celui du recyclage, le point 12, tout simplement parceque c'est un point sur lequel tout un chacun peut agir facilement.

La deuxième étape va être de définir plus clairement ma problématique. J'ai choisi de me concentrer sur les déchets d'impression 3D.
En effet, les imprimantes 3D sont de plus en plus populaires et démocratique. En conséquence, la quantité de déchet produit par celles-ci augmente tous les jours un peu plus.

Ma question est donc la suivante : Que faire de ces déchets, comment les traiter et les valoriserde manière utile, durable et en circuit court ?

## Une série de question

Maintenant que j'ai une question relativement claire sur laquelle me pencher, de nombreuses questions annexes se posent automatiquement quand on commence à réfléchir à ce qu'il faudra pour y répondre

* Sous quelle forme valoriser ? De la matière première, un produit fini, des consommables ?
* Quelles technique utiliser ? De l'agglomération, de la fonte ?
  * Quels additifs seraient utiliser pour de l'agglomération ?
  * Quelles sont les contraintes de volume, de coût, de sécurité pour la fonte ?
* Est-ce qu'on peut mélanger les plastiques différents ?
* A quelle échelle faire ce recyclage ? Individuel, fablab, ville ?
* Sous quelle forme doivent être les déchets pour être utiliser ? Copaux, pièce, peu-importe ?
* Quelles sont les limitations des produits obtenus ? alimentaire, décoratif, utilitaire ?
* Dans quels domaines peut-on utiliser ces produits ? Enfant, jardinage, alimentaire ?
* Les produits obtenus pourront-ils eux même être recycler ?
* D'autres sources de plasique peuvent-elles être envisagées ?
* Comment faire du moulage ?
* Y a t'il une production de fumée toxique ?
* Quelle est la quantité de déchets qui doivent être traités ?


## Des premières idées de solutions

Avant de répondre à toutes ces questions, j'ai réfléchit rapidement à quelques première idées de solutions qui existe déjà.

### Fabrication de filament

Il existe de nombreuses méthode de recyclage des déchets en bobine de filament. Des machines toutes faite qui font très bien le travail existe, mais elle coute malheureusement cher, et ont de plus le problème d'être assez volumineuse, donc pas du tout adaptée à une échelle très petite.
Il existe également des méthodes manuelles, mais celles-ci ont bien souvent l'inconvénient de ne pas être assez régulière pour permettre des impressions de qualité.

Que ce soit à l'aide de machine chère ou bien de machine faite maison, le principe est toujours le même.
* Mettre le plastique sous forme de petit Copaux
* Passez les copaux dans un extruder similaire à celui d'une imprimante 3D qui va chauffer le plastiques et le passer sous forme liquide
* utiliser un nozzle gros (+/- 2mm)
* Étendre le fil obtenu jusqu'à arriver à une épaisseur de 1.75mm (à l'aide de la gravité par exemple)
* Refroidir le fil et le mettre sous forme de bobine.

Bien entendu, une méthode plus simple consiste à "simplement" directement utiliser le fil générer à partir de copaux de plastique fondu, cela nécéssite une imprimante dont le plateau fait l'ensemble des mouvements.

### Fabrication de plaque de plastiques

Plutot que de réutiliser le plastique pour à nouveau des impressions, celui-ci pourrait être utiliser sous forme de plaque plus ou moins fine, pour de la découpe laser par exemple.

Pour ce faire, rien de plus simple, il faut faire un moule et y faire fondre les déchets, dans un four par exemple. Afin de limiter les aspérités et homogénéisé au mieux la plaque, il sera nécéssaire également d'appliquer une pression quand le plastique est encore chaud.

## Que font les autres ?

### Precious plastic

[Precious Plastic](https://preciousplastic.com/) est un peu LA référence dans le domaine du recyclage du plastique. Ils ont dévelloper un ensemble de machine dont les plans sont disponible pour toutes et tous. Les machines sont disponibles en plusieurs niveau de performance pour s'adapter aux différents besoins et aux échelles nécéssaires.
Il y a des machines pour toutes les étapes du recyclage, depuis la réduction en copaux du plastique jusqu'au façonnage du plastique en objet, en passant par la production de fils ou de plaques.

En plus de tous ces plans, il y a un grand espace communautaire ou tout le monde peut mettre en avant ses créations et ses modifications sur les machines de base.

Un gros point négatif de toutes les machines est leur coût qui peut rapidement être très cher (plus de 1000€ pour les modèles les plus basiques)

### Des méthodes plus artisanales

De nombreuses autres personnes ont déjà réfléchit au problème du recyclage du plastique. On voit rapidement qu'avec les moyens du bord, de l'huile de coude et de l'imagination on peut déjà arriver à de chouette résultats comme des pendentifs, des porte-clés ou des sous-verres comme nous le démontre les [Brothers Make](https://www.youtube.com/watch?v=FpsXvO5LsTY) sur leur chaine youtube.

Le défaut souvent présent dans les méthodes les plus artisanales est le manque de régularité des produits obtenus. Le résultat va fort dépendre de la dextérité de chacun, de la patience qu'on accorde au projet et du matériel à disposition.

## Des plaques de plastiques

J'ai décidé de me concentrer sur la dernière idée, le recyclage des déchets plastique des imprimantes 3D sous forme de plaques, pouvant servir par exemple de matière première pour une utilisation à la découpeuse laser. Cette idée étant très proche de celle d'Anaïs, la suite se fera en binôme.


### L'idée de base

L'idée de base peut être séparée, un peu comme une recette de cuisine, en plusieurs étapes :

- Récolte du plastique
- Préparation du plastique
- Fonte du plastique
- Moulage et traitement du plastique
- Utilisation du plastique

#### Récolte du plastique

Cette étape est sans doutes la plus simple, étant donné que c'est notre problématique. En effet, que ce soit chez nous ou au fablab, nous avons une source qui, même si on ne le veut pas, se remplit de tous les supports et erreurs de nos impressions.

#### Préparation du plastique

On peut entendre trois choses par "Préparation du plastique".
La première est la forme sous laquelle on va mettre le plastique. Les [modèle traditionnel de machine servant à recycler le plastique sous forme de plaques](https://community.preciousplastic.com/academy/build/sheetpress) demandent en général que le plastique d'entrée soit sous forme de copeaux. On verra par la suite que ce n'est pas forcément une nécéssité, mais que cela peut aider à l'homogénéisation des plaques obtenues. Nous avons décidé dans un premier temps de ne pas réduire en copeaux nos déchets de plastique.

La deuxième forme de préparation que l'on peut envisager est plus estétique. Je parle ici d'un tri des couleurs de plastique que l'on va utiliser, soit de manière monochrome, soit avec des associations de couleurs qui rendent bien ensemble.

La dernière forme de préparation est la plus importante, un tri par type de plastique. Le plastique le plus courant au fablab et chez les particuliers est le PLA, mais d'autres plastique peuvent aussi être utilisés. Il est important pour nous de bien séparer ces différents plastiques, ceux-ci n'ayants pas les mêmes caractéristiques.

#### Fonte du plastique

Nous allons nous concentrer sur le PLA, qui est à notre portée le plus abondant. Comme on le sait, dans les imprimantes 3D, le PLA est amené à une température de maximum 220°C, mais le PLA commence déjà à fondre dès 175°C.

Afin de limiter le risque de brûler les plus fins des bouts de PLA, nous allons limiter la température à 200°C.

Pour faire chauffer notre plastique, nous allons simplement le placer dans un four, sur une surface la moins adhérente possible, du papier sulfurisé par exemple.

#### Moulage et traitement du plastique

Une fois le plastique chaud, la course contre la montre commence. On a moins d'une minute avant que le plastique ne soit plus maléable, il ne faut pas trainer.

L'idée est de placer le plastique fondu dans un moule, que l'on va fermer par une positif du moule et que l'on va presser. Le moule a pour objectif de définir la forme de notre plaque, et la pression est là pour deux raisons. La première est d'uniformiser l'épaisseur de la plaque, la deuxième est de limiter la présence d'air dans la plaque.

#### Utilisation du plastique

Une fois notre plaque obtenue, elle peut servir de matière première. Certaines applications l'utilisent comme telle, pour par exemple faire des tuiles de toit. L'application que nous visons est l'utilisation des plaques avec une découpeuse laser, comme on utiliserait une plaque de plexiglass.

### La preuve de concept

#### La préparation de la plaque

Avant de se lancer dans la construction d'une réelle presse et de passer à une vraie mise en pratique, nous avons fait un petit essai pour vérifier la faisabilité de notre projet.

Pour ce faire, nous avons pris une petite poignée de reste de PLA.

![](../images/projet/PLA_non_fondu.jpg)

Après un premier passage au four à 220°C pendant deux minutes suivi d'un passage au rouleau à patisserie, deux conclusions faciles se sont impossées.
Premièrement, 220°C est une température trop élevée, en effet, les plus fins filaments de PLA ont brulé au lieu de fondre.
Deuxièmement, 2 minutes n'est pas suffisant pour homogénéiser suffisament le PLA avec des pièces assez grosses encore présentes.

![](../images/projet/PLA_premier_passage.jpg)

Un second passage fût donc nécéssaire afin de bien tout homogénéiser.
Après un nouveau passage sous le rouleau à patisserie, une plaque relativement homogène est obtenue. On voit donc qu'une pression relativement faible est déjà suffisante pour avoir un résultat convenable. La plaque obtenue a une épaisseur légèrement variable, mais le plastique est assez homogène.

![](../images/projet/PLA_resultat.jpg)

#### L'utilisation de la plaque.

Maintenant que nous avons une plaque, nous avons voulu voir ce qui pouvait en être fait.

Pour ce faire, nous avons téléchargé un simple logo du FabLab, et nous avons essayé de le graver à la découpeuse laser, avant de le découper de la plaque.
La plaque étant relativement épaisse, et la découpeuse utilisée étant la plus faible, plusieurs passages ont été nécéssaire, mais le résultat final est plus que correct.

![](../images/projet/PLA_decoupe.jpg)

Les utilisations de ces plaques sont donc infinies et limitée seulement par l'imagination de tout un chacun !

## La mise en pratique finale

Maintenant que nous savons que tout fonctionne, il est grand temps se lancer dans la conception d'un prototype complet et fonctionnel !

Notre source de plastique sera les bacs de tri de salle des imprimantes du FabLab ULB qui sont aujourd'hui plus que bien remplis.

Pour chauffer notre plastique à 200°C, nous allons utiliser le four présent au FabLab qui, ça tombe bien, peut chauffer jusqu'à 205°C.

Pour la presse, nous allons la faire nous même, en construisant un système à base de structure solide en bois et de cric de voiture.


### Le four

Le four présent au fablab est simple d'utilisation. Il suffit d'allumer la ventilation avec l'interrupteur vert puis d'allumer le four avec l'interrupteur rouge (de toute façon le four ne s'allume pas si la ventilation est éteinte).

![](../images/projet/oven.jpg)

Nous avons trouvé au fablab une plaque de four dans laquelle mettre notre papier cuisson et nos déchets de plastique.
Nous avons également trouvé une paire de gants résistants a la chaleur pour manipuler cette plaque sans danger.

Nous sommes maintenant prêts a faire fondre notre plastique.

![](../images/projet/before_oven.jpg)

La porte du four ne fermant pas très bien et le volume du four étant particulièrement grand, le four a mis une heure et demie à atteindre les 200°C. Heureusement, nous avions mis le plastique a l'intérieur des le debut, il a donc pu commencer a fondre des les 175°C.
Nous avons sorti rapidement le plastique une fois les 200°C atteints car nous avions plus beaucoup de temps disponible.
Nous l'avons alors immédiatement mis dans la presse.

### La presse

Nous avons fabriqué une presse en bois à partir d'une presse déjà présente au FabLab, qu'il a fallu remettre en état.
Son principe est simple, on met ce que l'on souhaite presser entre les deux plaques de bois horizontales, et on vient faire pression dessus à l'aide du cric.

![](../images/projet/press2.jpg)

### Le moule

Entre les deux planches horizontales de la presse se trouve un moule, qui permet de faire des feuilles de plastique bien homogènes. Nous avons fabriqué ce moule en découpant un rectangle dans une planche de bois à la découpeuse laser.L'exterieur de la découpe a servi a a faire les contours du moule, et l'interieur sert a faire le positif, positionné sur la plaque supérieure de la presse.

![](../images/projet/wood.jpg)
![](../images/projet/press.jpg)

### Le résultat

Après avoir mis notre plastique fondu dans la presse et fait pression, nous avons obtenu ce résultat :

![](../images/projet/after_oven.jpg)

Nous n'avons pas réussi a obtenir une plaque, parce que nous n'avons pas mis assez de plastique dans le four. Cependant le plastique a quand même bien fondu, et est bien aplati, donc on imagine qu'en en mettant une plus grande quantité dans le four ça peut marcher.

## BOM - Bill of Materials

_Rapide estimation en un tableau récapitulatif des couts de notre implémentation si on devait la refaire de zéro._

Pour ce projet, un des objectifs était de ne rien dépenser. Il a été atteint car nous avons réussi à n'utiliser que des matériaux de récupération. Cependant, sans avoir accès a ces matériaux, voici la BOM (Bill of materials = liste des couts) :

| Objet                                                                                                                                                                                                                                                                                         | Cout             |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| [Cric](https://www.cdiscount.com/auto/outillage-atelier/cric-hydraulique-de-bouteille-de-2/f-1330501-pri8434852165778.html?idOffre=401490950#cm_rr=FP:7583423:SP:CAR)                                                                                                                         | 22€              |
| [Bois pour le moule](https://www.leroymerlin.fr/produits/menuiserie/panneau-bois-tablette-etagere-tasseau-moulure-et-plinthe/panneau-bois-agglomere-mdf/panneau-contreplaque-okoume-twin-1200-x-400-x-10-mm-88105325.html)                                                                    | 18.30€           |
| [Planches de bois pour l'armature](https://www.laboutiquedubois.com/avives-douglas-bruts-certifie-pefc-100-679.html)                                                                                                                                                                          | 33.88€ (pour 4)  |
| [Planches pour faire les plaques horizontales](https://www.leroymerlin.fr/produits/menuiserie/panneau-bois-tablette-etagere-tasseau-moulure-et-plinthe/tasseau-planche-et-equerre-de-fixation/planche-et-latte-a-sommier/planche-sapin-petits-noeuds-brut-25-x-150-mm-l-2-4-m-68189520.html#) | 77.40 € (pour 6) |
| [vis](https://www.leroymerlin.fr/produits/quincaillerie/cheville-vis-clou-et-boulon/vis/vis-a-bois/lot-de-35-vis-acier-tete-fraisee-standers-diam-4-mm-x-l-16-mm-82231803.html)                                                                                                               | 3.30€            |
| [Papier sulfurisé](https://www.cdiscount.com/au-quotidien/hygiene-soin-beaute/papier-de-cuisson-15x0-30-m/f-127020211-sch3264221612863.html#mpos=0cd)                                                                                                                                         | 5.45€            |
| TOTAL                                                                                                                                                                                                                                                                                         | 160.33€          |

Le total est élevé mais la plupart de ces matériaux peuvent être trouvés en récupération assez facilement, ce qui permettra de réduire facilement le coût.

## Amélioration

Un des problèmes majeur de notre presse est qu'elle ne peut pas s'utiliser seul. La faire plus légère serait une belle amélioration, pour pouvoir soulever la partie haute d'une main et mettre le plastique a l'interieur de l'autre main.

Un problème que nous avons rencontré est que le four ne comporte pas de vitre, et il était donc impossible de voir a quel point notre plastique avait fondu avant de le sortir.

De nombreuses améliorations sont également possible sur la structure même de la presse avec moins de jeu entre la plaque mobile et les pilliers, plus de résistance à la pression de la barre horizontale, ou une plus grande facilité à modifier le moule utilisé.
