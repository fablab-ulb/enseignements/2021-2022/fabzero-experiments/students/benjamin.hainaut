Hello!

This is not an example blog for the FabZero-Experiments Course anymore, it's my personal introduction page.

I can edit it on [Gitlab](http://gitlab.fabcloud.org). The software used turns simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

Each time I change a page using the Gitlab interface the site is rebuilt and all the changes published in few minutes (or not, more likely during the night Tuesday and Wednesday).

This is not your site, so do not go on and edit this page clicking the Gitlab link on the upper right, you do not have the credentials anyway.

No worries, I can not break anything, even though I tried. All the changes I make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that I have all the different versions of my page saved and available all the time in the Gitlab interface.

## About me

![](images/avatar-photo.jpg)

Hi! I am Benjamin, a 24 years old student in biomedical science engineering. I live and have always lived in Brussels, capital of Belgium and Europe. I like reading medieval fantasy books and playing video games, but do not allocated those enough time nowadays.

At the university, I took a liking in student engagement and representation. I am the president of the Polytechnic student board (BEP), coordinator in the student council (BEA) and invested in approximately all instances in the Faculty.

## My background

I have always been interested in everything related to science, informatics and new technologies. I have done my elementary year in École 17 in Schaerbeek, and my secondary school in Athénée Fernand Blum, in the option Math and Sciences. I was very interested in computer science studies, but tried the entrance exam of Polytech, and as I passed, I have stayed in Polytech ever since.

## Previous work

Through the years at Polytech, I have had the opportunity to tackle different projects, for my academic cursus or for my personal interest.

### Game of Drone

In 2015, I work with a group of 5 other students to design and prototype a quadrimotor drone from scratch. It was a very multidisciplinary project where I learned about material resistance, electronics, programming, team work, regulators and the harsh reality that if something works fine in theory, it is not always as easy in practice.

### Beer brewing

In 2017, I brew my own beer, from the recipe design to the final step, the first sip. I also designed a  heat exchanger apt to sustain a huge scale up of production. This was also a school project, but since then I have brewed twice at home for my pleasure.

### RFID cash drawer

For the polytechnic studennt board, we develloped a automatic cash drawer using RFID cards. We used an arduino, designed all the circuit and reversed engineered the working of the cash drawer, and of course programmed the arduino.

### Covid detection in Chest CT-Scan

My MA1 project was to detect a Covid-19 patient using Chest CT-Scan images. I did it using a convolutionnal neural network, and go results of approximately 80% of correct classification.
It is important to note that the aim of this is not to replace a doctor but more to ease his work and guide him.
