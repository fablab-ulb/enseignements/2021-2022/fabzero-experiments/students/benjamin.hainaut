# 1. Documentation

This week I worked on defining my final project idea and started to getting used to the documentation process and the tools used to do so.


## Git
The first step is to get used to the utilisation of git.

> Git is a version control software. It is used to track any changes in a repository and keep all those changes in memory. Every devices connected to the project keeps every informations in memory, it is then really important to keep the repository size small (storing data on-line consume a lot of energy<sup>[1](https://iopscience.iop.org/article/10.1088/1748-9326/abfba1)</sup>).

There are some step to get fully ready, first, the installation

* Download Git on it's [website](https://git-scm.com/)
* Run the installer

It's already done ! Now you can use git. But it is not yet ready to collaborate with Gitlab... You need a few more step to be able to prove to Gitlab that you are you. You will need a pair of ssh keys, a public and private one, and you will have to give the first one to Gitlab, and preciously keep the second one for you. Here are the steps needed.

> SSH (Secure Shell) is a network protocol used for operating network services securely over an unsecured network. It allows you to communicate safely with a distant server.

* Right click anywhere and launch git BASH.
* Type the following command
```
ssh-keygen -t rsa
```
* Simply follow the instructions on screen
  * Choose the name the pair of keys will have
  * Choose a pass phrase to secure your keys

You now have your pair of keys. You have to add the public one on your Gitlab account. There is still one last step, add your key to git BASH.
 ```
eval $(ssh-agent -s)
ssh-add <directory to private SSH key>

nano ~/.ssh/config
 ```
In the file that just opened, type
```
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlab_com_rsa
```

You can now clone the project on your computer

```
git clone <ssh clone URL>
```
You can get the URL directly on gitlab, on the top right corner of the page (clone button)


## Markdown
This web page is done using Markdown, a lightweight markup language. Using any text editor, you can create a file that will be displayed with a nice format.

I personally chose to work with Atom, a text editor with a preview option that is really useful, with the advantage of being open-source.

> Open-source project give to everyone their source code, for you to work with to add functionalities, adapt to your needs, correct bugs, ...

### Update the pages
I can now write the pages of my website on my laptop, but it's not automatically updated on the class website. To do it, you need to use some commands on the Git BASH.

```
git pull
git add .
git commit <Informations about the modifications done>
git push
```

Git pull is there to check there has been no changes on the server side. Then you add all the files of the folder to the process, describe the modifications done and then send those data to the server.

### To go further
Markdown allows you to do a lot more than what is done on this page. You can integrate images and videos from different sources, you can change the theme, make tables, ...


## Les images

Une image prend rapidement beaucoup de place, et dans l'idée de limiter l'utilisation des espaces de stockage numérique (ils ont un impact non négligable sur notre planète de part les matières première nécéssaire à la mise en place des centres de stockage et de part la nécéssité de refroidir constamment les serveurs), il est important de limiter la taille des images que nous utilisons sur ce site.

Une image normale de smartphone a une très haute résolution, et est pas conséquent très lourde. Cette qualité est nécéssaire pour des images à but artistique, mais ne sera dans la majorité des cas pas nécéssaire pour moi ici.  Il faut donc retravailler ces images pour faire diminuer leur poids, qui par défaut oscille entre 1 et 10Mo par image !

### Gimp, paint, Photoshop

Une des solutions est la plus évidente et facile pour monsieur/madame tout le monde, des programmes de retouche d'image. Un de ces nombreux programme est Gimp. Gimp à l'avantage d'être open-source. En ouvrant l'image dont je veux réduire la taille, il me suffit d'aller dans le menu "Image", et ensuite de cliquer sur "Echelle et taille de l'image". Dans le menu qui va s'ouvrir, je peux choisir la taille de l'image que je souhaite.

La dernière étape est d'exporter l'image dans le format souhaité. Les formats traditionnel sont le PNG et JPEG. Le PNG est un format de compression sans perte, donc la qualité de l'image sera conservée, mais le poid de l'image ne va pas forcément diminuer énormément. Le JPEG quant à lui est une copression avec perte, la qualité sera donc légérement affectée, mais le poid sera très fortement diminué (un facteur 10 n'est pas inimaginable !)

Gimp permet également de faire énormément de manipulation sur les images (rotation, traitement des couleurs, découpe, collage, ...), à condition de prendre le temps nécéssaire à la maitrise réelle de toutes ces fonctionnalités plus avancées.

### GraphicsMagick

La deuxième solution est d'utiliser des programmes moins chouette pour les utilisateur novice, sans interface graphique. GraphicsMagick est un de ces programmes. Il permet de la même façon que Gimp de réduire le poids des images, et de faire également quelques opérations plus variée (redimensionnement, changement de couleur, de format, concaténation d'images, ...)

La grosse différence avec Gimp est simplement l'absence d'interface grapique, tout se fait donc en ligne de commande. Voici un example de commande, en considérant une image qui fait originalement 200x200 pixels qu'on va nommer IN.PNG

```
gm convert -resize 64x64 IN.PNG OUT.JPG
```

Cette commande va simplement convertir l'image de base en une image plus petite, et dans un autre format.

Toutes les informations sur ce qu'il est possible de faire, et comment le faire, peuvent être retrouvées directement [sur le site de GraphicsMagick](http://www.graphicsmagick.org/GraphicsMagick.html)

Le gros avantage de GraphicsMagick est la rapidité à laquelle le travail peut être fait, surtout parcequ'il permet un traitement en groupe d'images auquelles on veut appliquer le même traitement.
