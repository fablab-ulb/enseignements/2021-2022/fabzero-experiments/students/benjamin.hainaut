# 3. Découpe laser

## Les CNC laser
Les CNC sont utilisées pour couper ou graver divers matériaux.

### Consignes de sécurité
* Il faut savoir le matériel que tu vas couper, certains peuvent prendre facilement feu.
* Il faut toujours activer l'air comprimé et l'extracteur de fumée.
* Il faut savoir où se trouve le bouton stop d'urgence
* Il faut savoir où est l'extincteur à CO2, le seul qu'on peut utliser pour ces machines (limite la casse)
* Il toujours rester à coté de la machine quand elle est en fonctionnement
* Il ne faut pas fixer le laser, ça peut rendre aveugle
* Il ne faut pas ouvrir la machine tant qu'il a de la fumée dedans


### Matériaux recommandé
* Bois contreplaqué (plywood, multiplex, ...). Attention, il y a parfois un comportement très différent d'un bois à l'autre (dépend notament de la glue présente dans le bois)
* Acrylique (PMMA, plexiglass), le plus simple à couper au laser
* Papier, carton, ... Attention que ça peut rapidement prendre feu
* Textiles (! sans chlorine)

### Matériaux non recommandé
* MDF, il produit trop de fumée
* ABS, Polystyrene, ils fument et fondent
* Composite à base de fibre (fibre de verre, de carbon, ...), ils produisent de la poussière nocive
* Métaux, ils ne sont pas découpable au laser (ça peut-être possible pour de très très fine couche)
* Cartons épais (3 couche ondulée), ils prennent feu

### Matériaux interdit
* PVC, il produit des fumées acides
* Cuivre, il réfléchit à 100% le laser
* PTFE, il produit des fumées toxiques
* Résine (époxy), il produit des fumées toxiques
* Vynil, il contient de la chlorine et produit donc encore une fois des fumées acides
* Cuir animal, simplement parcequ'il produit une odeur trop désagréable

### focus
Il faut bien mettre le focus selon nos besoins, en fonction de l'épaisseur et de si l'on souhaite faire de la gravure ou de la découpe. Pour couper, on essaye de placer le focus à la moitié de l'épaisseur du matériaux.

### Drift
Il ne pas aller trop vite, sinon il y aura du drift. Au lieu d'avoir de belles lignes bien droite, il va y avoir des petites ascillations non voulue.

## Laser kerf
Il faut connaitre le diamètre du rayon laser, c'est d'autant plus important pour les pièces qui s'emboitent. Typiquement, il y a 1mm de jeu si on ne fait pas attention à ce point.


## Les machine du Fablab

### Epilog Fusion Pro 32

#### Caractéristiques
* Surface de 81x50cm
* Hauteur de 31cm
* Puissance de 60W

#### Utilisation
Tu fais ton dessin sur InkScape, un programme de dessin vectoriel open-source, puis tu l'imprimes vers la découpeuse. Ensuite il faut ouvrir le programme de l'imprimante ou tu peux régler les paramètres du laser en fonction de la couleur du dessin
Les différentes options sont la découpe ou la gravure, la puissance, en mode raster ou vector (raster grave la surface, pas juste les contours)

Un dernier parametre est le plunger qui est utilisé pour déffinir le focus du laser et avoir une plus grande précision.


### Lasersaut

#### Caractéristiques
* surface très grande (122x61cm)
* rapide (6000mm/min, mais on se limite à 2000)
* puissance de 100W

#### Utilisation
Tout se passe sur programme open source mais celui-ci n'est pas toujours le plus efficace (DriverBoard)
Le focus du laser est a set soi-même.
Il ne faut pas oublier d'allumer le watercooling, ainsi que l'extracteur de fumée et l'air compressé !


### Full Spetrum muse

#### Caractéristiques
* Surface petite (50x30cm)
* Puissance de 45W
* Connection via Wifi

#### Utilisation
On utilise un autre programme encore, Retina Engrave. Le PC doit être connecté en réseau à la machine.
Lors de l'exportation du fichier, il ya deux fichier produit, il faut supprimer le fichier matriciel et utiliser le vectoriel.



## Group assignment - Epilog Fusion Pro 32

Our first assignment this week was to choose a material and determine the best parameters for our specific machine (speed, power, kerf, ...)

We will work with SVG files (Scalable Vector Graphics). The program we will use is InkScape, but we could also use OpenSCAD.

### Create the test bench

The first step is to sketch the piece we want to cut. We decide to do a grid of 5x5 squares on InkScape.

To be able to use it with the laser cutter, we need to use a vectorial image. So we export it this way:

![ ](../images/module03/vectorial.jpg)

Next, we need to color each square outline. Each one will have a different color because we want to be able to test each combination of force and speed at once.

![ ](../images/module03/colorsquare.jpg)

We chose to use a gray gradient for our different colors.

We export our sketch into SVG. Using a USB key we open our file on the Epilog's computer.

### Cut the test bench (first try)

We open the file and we go to *FILE>PRINT* and we select the machine.

![ ](../images/module03/fileprint.jpg)

Next, we get into the Epilog's Software. We need now to set the parameters.

First, pushing on the sketch, we have the possibility to split this sketch by colors. Then, we can determine the parameters for each subpart (each part of the sketch having a different color).

For each one, we choose those parameters :

- **process type** : *vector*
- **speed** : we go from 10 to 90 (step of 20)
- **power** : we go from 10 to 90 (step of 20)

Next, we choose to go with orange cardboard. We put it into the laser cutter (careful to put it flat in the machine to keep a constant distance between the cardboard and the laser).

On the software, we check with the video how to place the sketch. After, we check that the borders are well placed (those borders defines the region the laser can go into).

We choose for the parameter **Auto Focus** : *plunger*. Then, we *print* to add the file to the Epilog machine.

Comme nous pouvons le voir sur la photo suivante, nous avons allumé l'extracteur de fumée, nous avons activé la machine grâce à la clé, nous avons bien fermé la machine en appuyant dessus et en vérifiant que les LED correspondantes étaient bien allumées.

![ ](../images/module03/careful.jpg)

We choose the file on the Epilog machine and we start.

![ ](../images/module03/start.jpg)

After having cut the piece, we can delete the file from the machine.

![ ](../images/module03/test1.jpg)

The result is a bit disappointing, almost every square is simply totally cut through. It is simply because the cardboard is too thin and the laser set on a too high power.

### Cut the test bench (second try)

For our second try, we decided to focus ourself on a small part of the power range.

![ ](../images/module03/drawing.jpg)

This time, the parameters are :

- **process type** : *vector*
- **speed** : we go from 30 to 90 (step of 15)
- **power** : we go from 5 to 25 (step of 5)

![ ](../images/module03/test2.jpg)

The result is much more conclusive. Some of the square were very loose and fell down as soon as we picked up the test bench, but we have much more samples to study. We can see that if we go too slow, it will inevitably cut through the paper.

### cut the test bench (third try)

For the second part of the assignment, the Kirigamis, we still lack one important information, what is are the best parameters to be able to easily fold our cardboard without simply cutting it.

We designed a simple rectangle with 5 lines, each having different parameters. We chose those parameters based on our previous test bench, we selected the squares that were not cut through but still quite deeply cut.

![ ](../images/module03/PLI.jpg)

The parameters are :

- **Process type** : *vector*
- **Speed** : 30, 60, 75, 90, 90
- **Power** : 5, 10, 10, 10, 15

Every cut hangs on correctly, but the best parameters pair seems to be (90,10), as it seems to be a bit more resilient.



## Devoir personnel - Un kirigami

Les Kirigamis sont des oeuvres d'art en papier issus de la culture Japonaise, proche cousin des origamis (oeuvre en papier n'utilisant que des pliage), ils se différencient de ces derniers de part la possibilité supplémentaire de couper le papier.

De notre côté, nous n'allons pas choisir la méthode habituelle, à savoir des ciseaux et un cutter, mais allons lui préférer l'utilisation d'une méthode plus robuste dans sa répétabilité, la découpeuse laser.

J'aurais aimé réaliser un kirigami nécéssitant le pliage de la feuille en deux et représentant grâce au différentes découpes et pliage un batiment, mais malheureusement, ce type de kirigami demande des pliages dans les deux sens, et la découpe laser ne permet facilement que de faire des plis dans un sens, il faudra donc se contenter de plus simple.

![ ](../images/module03/Pichim1.jpg)

Le modèle plus simple que j'ai choisi de faire est un simple cube, dont j'ai designé les arrêtes via OpenSCAD.

```
// File : Kirigami.scad
//
// Author : Benjamin Hainaut
//
// Date : 21 avril 2022
//
// License : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
module emptySquare(){
    polygon(points=[[0,0],[10,0],[10,10],[0,10],[0.1,0.1],[9.9,0.1],[9.9,9.9],[0.1,9.9]], paths=[[0,1,2,3],[4,5,6,7]]);
}

union(){
    translate([0,0,0])emptySquare();
    translate([10,0,0])emptySquare();
    translate([0,10,0])emptySquare();
    translate([0,-10,0])emptySquare();
    translate([20,0,0])emptySquare();
    translate([-10,0,0])emptySquare();
    translate([-4,-4,0])rotate([0,0,-45])cube([sqrt(130),0.1,1], center = true);
    translate([-4,14,0])rotate([0,0,45])cube([sqrt(130),0.1,1], center = true);
    translate([-10,9,-0.5])rotate([0,0,-120])cube([sqrt(22),0.1,1], center = false);
    translate([-9.90,1,-0.5])rotate([0,0,120])cube([sqrt(22),0.1,1], center = false);
}
```
![](../images/module03/Kirigami.PNG)

La prochaine étape est de passer ce fichier dans InkScape, en l'exportant d'abord en format DXF, puis de l'exporter en SVG avant de passer simplement à la découpeuse. Les petits triangles supplémentaires permettrons simplement de faire tenir le cube plus facilement en place.

![](../images/module03/full.jpg)

En utilisant les même paramètres que présenté plus haut, j'obtients une bell découpe, avec un pliage facile et assez résistannt. Avec tois petits points de colle, le cude tient bien.
