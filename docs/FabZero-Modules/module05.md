# 5. Electronique 1 - Prototypage


## Qu'est-ce qu'un Arduino/micro-controleur ?

Un Arduino est un mini ordinateur qui peut excécuter toute une série de tâche, recevoir des informations des l'extérieur (INPUT), et transmettre des informations (OUTPUT).

Afin de faciliter la chose, il existe un IDE (interface de dévellopement) lié à Arduino qui permet d'y dévelloper son code, et de l'envoyer sur le micro-controleur

### Comment ne pas détruire un Arduino ?
Comme toujours, lorsqu'on utilise de l'électronique, il y a des risques. Il y a de nombreuses PIN sur un Arduino, toutes ne peuvent pas servir à la même chose.

![](../images/module05/arduinoPIN.jpg)

On voit sur le pin mapping que toutes les pin ont un numéro.
Il y a un groupe de PIN pour la gestion de l'alimentation/GND.
Il y a un groupe de PIN digitale (0 ou 1) et un groupe de PIN analogue (entre 0 et 5V).

Les PIN RX et TX sont utilisées pour la communication de data.

Le courant maximal qui peut passer dans chaque PIN est de 40mA (Mais il est mieux de limiter à 20mA). Par groupe de PIN, le maximum est de 100mA ==> on ne peut pas mettre de led sur toutes les PIN d'un groupe.

Si une PIN est définie en INPUT, il n'y a pas de connection avec les sources de tension, donc pas de danger. Il faut donc toujours faire attention a ne pas connecter son circuit comme si une PIN est en INPUT si la pin est en OUTPUT.

Il ne faut pas appliquer un voltage de plus de 5V sur n'importe quelle PIN.
Certaines PIN peuvent être modulée en fréquence pour l'OUTPUT, ça permet de simuler un signal analogique.

## Les différents modules

L'arduino seul peut déjà être utile, mais il vient avec énormément de modules complémentaire qui permettent d'échanger avec l'extérieur, que ce soit avec des INPUT supplémentaires (thermomètre, capteur de distance, d'humidité, de lumière, boutons, ...), ou avec les OUTPUT (moteurs, speaker, LED, ...)

Une liste complète de tous les modules existant serait bien évidemment trop longue, mais il est possible de se faire une déjà bien bonne idée de ce qui existe sur les [sites des constructeurs de ces modules](https://www.velleman.eu/products/list/?id=456378), velleman par exemple.
C'est également sur ces sites que se trouve les documentations techniques des modules, ainsi que souvent un code d'exemple de l'utilisation du module.

## Un exemple de base
En programmation, la première que tout le monde fait quand il commence, c'est afficher le fameux "Hello World". Avec un arduino, il n'y a par défaut pas d'affichage, donc l'équivalent au "Hello World" est de simplement faire clignoter une LED.

```
/* Blinking LED
 *  
 * Author : Benjamin Hainaut
 *
 * Date : 17/03/2022
 *
 * Licence : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
 */

#define ledPIN 8

void setup() {
  pinMode(ledPIN, OUTPUT);
}

void loop() {
 blink(1000);
}

int blink(int time_blink){
  digitalWrite(ledPIN, HIGH);
  delay(time_blink);
  digitalWrite(ledPIN, LOW);
  delay(time_blink);
  return 0;
}
```
Le résultat est simplement une petite LED bleue qui clignote toutes les secondes.
<video controls muted>
<source src="../images/module05/LED_OUT.mp4" type="video/mp4">
</video>

La partie void setup() sert à définir l'état des PIN que l'on va utiliser, ici on dit que la PIN 8 sera une sortie.

La partie void loop() est la boucle qui sera répétée infiniment. Ici elle est très vide tout simplement parceque j'ai défini une fonction blink() en dehors de la boucle. C'est une bonne pratique pour rendre ses codes plus modulaires et facilement compréhensibles.

La fonction blink() place simplement la PIN 8 en HIGH (sortie de 5V sur la PIN), attend un certain temps, puis remet la PIN 8 en LOW (sortie de 0V sur la PIN).

A noter la première ligne de code qui permet de définir que dans la suite du code, ledPIN vaut 8. Cela permet d'utiliser dans tous le code ledPIN, et si lors du montage du circuit on décide d'utiliser une autre PIN, on peut simplement changer la valeur assignée à ledPIN, il ne sera pas nécéssaire de changer la valeur partout dans le code.

## Pour aller un peu plus loin

L'avantage des différents modules est bien entendu qu'en plus de pouvoir nous montrer une sortie, on peut également interagir avec notre arduino avec différents capteurs; Ici j'ai ajouté un capteur de luminosité qui permet, comme on s'en doute, de détecter l'éclairement. Si jamais la luminosité détectée est faible, la LED va s'allumer, simulant une application potentielle d'éclaire automatisé.

```
/* Blinking LED 2
 *  
 * Author : Benjamin Hainaut
 *
 * Date : 17/03/2022
 *
 * Licence : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
 */

#define ledPIN 8

void setup() {

  pinMode(ledPIN, OUTPUT);
}

void loop() {
  int analogValue = analogRead(A0);
  if(analogValue < 500){
    digitalWrite(ledPIN, HIGH);
  } else {
    digitalWrite(ledPIN, LOW);
  }
  delay(100);
}
```

Le module supplémentaire utilisé est un simple capteur de luminosité, qui est une résitance variable dépendant du flux de lumière qui l'illumine. La résistance variable varie de 2MΩ (Noir) à 20kΩ (Pleine lumière).
S'il fait trop lumineux, la LED reste éteinte, s'il fait assez clair, elle s'allume.

L'entrée de l'information se fait via une des PIN d'entrée analogique et varie entre 0 et 5V. On est capable de mesure jusqu'à 1024 niveau d'illumination différents, mais dans la réalité, le capteur est assez peu précis, je me suis donc contenter de deux niveaux.

## Pour aller encore plus loin

Il est toujours possible d'aller plus loin. Le code suivant permet de lire via un module RFID des carte magnétique, puis de vérifer si cette carte est autorisée dans le système, et si c'est bien le cas, d'ouvrir la caisse enregistreuse.

```
/* CashDrawer
 *  
 * Author : Loric
 * Modified by Benjamin Hainaut
 * Modifié par le trésorier BEP 2019-2020
 *
 * Date : 15/11/2019
 *
 * Licence : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
 */
/*
*
* Some of the resources for this project: https://randomnerdtutorials.com/
*
*/

#include <SPI.h>
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance.


// -------------------------- LISTE DES UID DES DÉLÉGUÉS (voir git pour savoir comment en ajouter) -------------------- //
String delegue[28] ={ "1D 98 47 1E", //Maxence
            "1D 19 E9 AD", //Robin
            "19 G0 38 D5", //Loric
            "16 L9 67 F8", //Andrew
            "17 81 MB 26", //Gilles P
            "1D 1E 83 AD", //Tiago
            "1D BE 4J 1E", //Guillaume
            "1D 2D DF 1E", //Jules
            "10 DC 65 1E", //Pauline
            "16 8B 69 31", //Benjamin
            "16 46 5F 11", //Gilles V
            "16 C1 K5 11", //Delphine
            "16 56 68 11", //Chloé
            "16 95 63 11", //Gaby
            "16 09 G8 11", //Achille
            "1D 11 D7 AD"  //Thomas

};
// -------------------------------------------------------------------------------------------------------------------- //


void setup()
{
  Serial.begin(9600); // Initiate a serial communication
  SPI.begin();  // Initiate SPI bus

  mfrc522.PCD_Init(); // Initiate MFRC522

  Serial.println("Approximate your card to the reader...");
  Serial.println();


  pinMode(6, OUTPUT); // Relay
}


void loop()
{
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }

  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content= "";
  byte letter;

  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }

  Serial.println();

  content.toUpperCase();

  for (int i = 0; i<sizeof(delegue)/sizeof(delegue[0]); i++) // checks if the UID is in the list
  {
    if (content.substring(1) == delegue[i])
    {
      Serial.println("Authorized access");
      Serial.println();

      // Toggles on and off the relay
      digitalWrite(6, HIGH);
      delay(500);
      digitalWrite(6, LOW);
      break;
    }

    else if (delegue[i] == NULL)
    {
      Serial.println("Access denied");
      Serial.println();
      break;
    }
  }

  delay(1500); // waits before checking again
}
```
<video controls muted>
<source src="../images/module05/Caisse_OUT.mp4" type="video/mp4">
</video>
