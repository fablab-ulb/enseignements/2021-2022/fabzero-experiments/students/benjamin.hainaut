# 4. Impression 3D

Dans ce module, nous allons travailler avec les imprimantes 3D, mais avant de se lancer dans la pratique, on a commencer par une petite introduction sur les application actuelle de la technologie d'impression 3D dans le domaine médical.

## L'impression 3D dans le domaine médical

L'impression 3D permet un prototipage très rapide. Elle fût d'abord utilisée dans l'aviation et l'automobile, avec des impressions 3D en métal, avec une technique de poudre métallique déposée et fondue à coup de laser.

Cela a apporter au domaine un grand gain de temps et de poids pour certaines pièces. Aujourd'hui ces avancée permettent de penser à des voiture ou moto volantes.

Dorénavent, l'impression 3D est utilisée dans énormément de domaine. Cela va de la création de bien de consommation à la construction en passant par l'agroalimentaire, sans oublié le domaine de la santé.

Dans le domaine médical, on a aujourd'hui deux grande branches qui utilisent l'impression 3D, les appareil médicaux (Prothèses, orthèses, ...) et les médicaments.

Bien entendu, de nombreux domaine d'application implique aussi de nombreuses technologie différentes pour l'impression 3D :

* Impression 3D sur lit de poudre (Métal)
* Impression 3D goutte-sur-goutte (Pharma)
* Impression 3D par micro seringues pressurisées (Alimentaire)
* Impression 3D par dépôt du fondu (La plus connue du grand public)
* Stéréolithographie (SLA)
* Digital light processing (DLP)

Nous allons nous concentrer sur la plus connue, l'impression 3D par dépot du fondu.

## L'impression 3D par dépot du fondu

Le principe de ces imprimantes est d'utiliser des thermoplastiques sous forme de filament, ou de granulés. Le thermoplastiques est amené à température de fusion à la tête d'impression, le "nozzle", et est ensuite déposé à vitesse et quantité controlée selon un schéma. Ce schéma est en fait la traduction d'un modèle 3D en gCode, le langage universel des inmprimantes 3D et des CNC.

### La préparation du thermoplastique

Avant tout impression, il est important que le plastique soit le plus sec possible. En effet, la présence d'humidité va grandement influencer la qualité de l'impression avec l'apparition de bulle, et donc de trou dans la structure.

Pour retirer l'humidité, le plastique peut simplement être stocker pendant quelques heures dans un four à basse température.

### La préparation de l'imprimante

Pour s'assurer d'une impression correcte, il faut s'assurer que la tête de l'imprimante soit propre, que sa position soit bien calibrée et que le plateau soit bien plat.


## Le modèle 3D

### Design

On a vu dans le module 2 comment créer ses modèle 3D, c'est la première étape obligatoire pour toute impression.

### Slicing

Une fois qu'on a notre modèle, nous allons utiliser un programme de découpe en couche, un slicer. Celui-ci va prendre notre modèle 3D et selon différents paramètres que l'on doit définir.
Les paramètres à définir sont, au minimum :

* L'épaisseur des couches
> L'épaisseur recommandée est toujours la moitié du diamètre du nozzle. Il est possible d'aller plus haut, mais plus dur d'aller plus bas.
* La température du nozzle
> Cette température va dépendre du plastique utilisé, pour du PLA, on va se situer entre 175 et 200°C
* La température du plateau
> La température du plateau va influencer sur la qualité de l'adhérence de la première couche. En fonction de la température ambiante, il va falloir modifier celle-ci. On est souvent à un température avoisinant les 60°C
* Les vitesses de déplacement
> Au plus on souhaite avoir une impression de haute qualité, au plus la vitesse devra être lente. Si le résultat attendu peut être assez brut de décoffrage, il est possible d'accélérer l'impression en accélérant les déplacement de la tête. Attention à garder une vitesse suffisament lente pour permettre au plastique de se mettre en place et d'adhérer suffisament.
* Le remplissage de la pièce
> Il n'est que très rarement nécéssaire de remplir entièrement une pièce. Cela n'augmente pas ou presque pas ses caractéristique mécanique mais augmente par contre énormément le temps d'impressions. En général, un remplissage de 15 à 30% est emplement suffisant.
Plusieurs types de remplissages sont possible, ils ont tous différentes caractéristiques mécanique différentes dont il faut tenir compte suivant la nature du projet.
* Les supports
> On imprime des pièces qui peuvent parfois avoir de grande partie suspendue. Comme c'est du plastique fondu qui est utilisé, il est important qu'il y ai quand même un certain support pour éviter que toute notre pièce s'éffondre.

 Personnelement, j'utilise le logiciel de Prusa, PrusaSlicer. Dans celui-ci, il est possible de préciser le modèle de l'imprimante utiliser, et de nombreuses configuration de base sont déjà proposées.

## Ender 5 Pro

 Ayant une imprimante chez moi, et n'ayant que trop peu de temps disponible pour occuper le FabLab durant ses heures d'ouverture, j'ai décider de travailler sur mon imprimante, une Ender 5 Pro.

 Elle a un volume d'impression de 220x220x300mm et contrairement aux imprimantes Prusa présente au FabLab, c'est le plateau qui se déplace pour l'axe Z. La buse à ma disposition est de 0.4mm de diamètre.

## Projet de groupe

 Je n'ai malheureusement pas pu assister à la séance de ce module, et je n'ai donc pas pu faire avec mes camarades le torture test. Je l'ai donc réalisé chez moi, ce qui est encore mieux, étant donné que l'objectif de ce test est de déterminer les performances d'une imprimante précise.

 Ce torture test consiste en l'impression d'un modèle avec des angles d'impression, partie suspendue et de long pont qui permette de voir les limites de l'imprimante. On va utiliser la version mini du torture test disponible sur Thingiverse : [Mini Torture Test](https://www.thingiverse.com/thing:2806295)

## Projet personnel

 Le projet personnel de cette séance consiste à reprendre les pièces que nous avons designées lors du module 2, et des les assembler pour obtenir un mecanisme à base de flexlinks.

 Malheureusement pour moi, l'imprimante que je comptais utilisé a décidé de m'abandonner en dernière minute. En effet, lors de chaque impression, après un lapse de temps plus ou moins long, le nozzle de l'imprimante se bouche.

 Cela peut être du à plusieurs source, mais j'en ai identifiés deux. La première est un potentiel légé déréglage de l'extrudeur qui ne pousse peut-être plus le fil vers la tête chauffante à une bonne vitesse, et le décallage léger entraine à la longue un manque de plastique qui du coup refroidit et bloque la sortie.

 La deuxième possibilité à laquelle j'ai pensé est l'humidité. En effet, je n'ai pas eu le temps ces derniers mois d'utiliser mon imprimante, et le fil à donc eu le temps de s'imbiber d'humidité. L'humidité crée dans le nozzle des bulles qui peuvent bloquer le filament. Pour parrer à ce problème, j'ai placé ma bobine au four pendant 6h à 45°C, mais cela n'a malheureusement pas résolu mes problèmes.

 Au vu de l'avancement dans l'année, j'ai choisi de ne pas pousser plus loin mes efforts et d'avancer plus sur mon projets final et mes autres cours ou responsabilités à l'université.
