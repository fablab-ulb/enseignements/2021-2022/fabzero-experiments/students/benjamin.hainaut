# 6. Electronique 2 - Fabrication

Un arduino c'est bien, mais ce n'est pas toujours le mieux. Parfois c'est trop complet et trop cher pour une utilisation simple, et c'est également une vielle technologie qui est encore sur le marché de nos jours simplement grâce à la communauté immense qui s'est formée autour d'Arduino avec de nombreuses application facile à mettre en place pour toutes et tous.

Une solution à tout ces problèmes est de tout simplement faire notre propre Arduino.

Nous allons utiliser un microprocesseur Atmel SAMD11C, qui est un microprocesseur 32bits (Arduino est basé sur un microprocesseur 8bits), ce qui est très bien au niveau performance et capacité, mais il y a quand même un gros inconvéniant, il n'y a que 16kB de mémoire, dont la majeur partie est occupée par les librairies de communication USB (dont on a besoin pour pouvoir programmer le microprocesseur). Ce microprocesseur a quand même été choisi parce qu'il intègre nativement la gestion des USB.

## Les composants

Les composants sont très simple, en plus du microprocesseur, nous avons :
* Une LED rouge qui sera connectée directement à l'alimentation
* Une LED verte qui sera sur la carte
* Deux capacités
* Un régulateur
* Trois résistances
* Deux set de PIN

Deux des trois résistances sont là pour protéger les LED, la dernière à protéger le régulateur, les PIN sont là pour faciliter les connections avec l'extérieur.

## La mise en place

C'est bien joli d'avoir tous nos composants, mais il faut maintenant les lier ensembles.

Malheureusement, je n'ai pas pensé sur le moment même à prendre des photos petit à petit, mais voici quand même le résultat final une fois toutes les pièces en place et soudée fermement.

![](../images/module06/Arduino.jpg)

La soudure n'est ici pas la plus simple des chose, les composants étant relativement et de surface. Le principe est quand même simple, il suffit d'apporter un petit peu de métal chauffé (étaint), de chauffer la pate de la pièce qu'on veut souder et son support, et puis de tout mettre ensemble.

Il est plus que probable d'avoir certain raté, dans ces cas là, un aspirateur à étaint peut être utilisé pour retirer le métal ajouté et recommencer. Il faut bien faire attention à ne pas trop chauffer les composants, cela pourait les endommager.

L'étape suivante est de mettre en place le bootloader qui permet à mon ordinateur de reconnaitre la carte.

## La programmation

Normalement maintenant tout fonctionne bien !
Il suffit donc de configurer correctement l'IDE Arduino (ajouter le type de carte qu'il faut et bien régler les paramètres), puis de faire du code.

Malheureusement, un faux contact durant le téléversement d'un code de test (Celui d'Andrew que je vais joindre ci-dessous), à corrompu mon bootloader. Donc je ne peux pas vérifier que tout marche, il faut reseter le microprocesseur et y remettre le bootloader.

```
/*
 *   FILE    : blink.ino
 *   AUTHOR  : Andrew KARAM <karamyo@hotmail.com>
 *   DATE    : 2022-02-28
 *   license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)
*/

const int led = 15;
const int blink_speed = 200;

void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(blink_speed);                       // wait for half a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(blink_speed);                       // wait for half a second
}
```
