# 2. Conception Assistée par Ordinateur

La conception assitée par ordinateur est un outil indispensable de nos jours pour dévelloper rapidement ses projets sans perdre de temps avec des prototypes physique fait entièrement à la main. Ici, après avoir modelisé notre objet, on peut utiliser des machines pour passer de notre modèle à un objet physique.

Cette semaine nous n'allons pas encore regarder les machines, celles-ci viendrons les semaines à venir, nous allons dans un premier temps nous intérrésser aux outils de conception numérique.

## En deux dimensions
En deux dimensions, on a deux option. On peut soit travailler sur des pixels, soit travailler en vecteur.

En pixels, c'est plus intuitif, on dessine chaque point de couleur comme il apparait sur l'écran, mais l'inconvénient est qu'on est donc limité à une certaine résolution. On poura jamais zoomer de manière infinie.

La solution à ce problème, c'est de ne plus travailler avec des pixels, mais avec des vecteurs. En vectoriel, on va se rappeler de la forme de nos objects, pas des pixels. Les dessins vectoriels peuvent donc être mis à toutes échelles.

Un logiciel opensource qui permet de faire du dessin est Inkscape. Nous n'allons pas pousser plus loin aujourd'hui le dessin 2D et revenir dessus lors du module 4 et la découpe laser.


## En trois dimensions

En trois dimensions, c'est la même chose. On peut travailler soit avec des voxels, l'équivalent des pixels de la 2D, mais on se retrouve alors avec le même problème.

La solution est la même, plutôt que de travailler avec sur les voxels, on va s'atteler à décrire la forme de nos objets.

On a de nombreuses options à notre disposition. Certaines solutions ne sont pas vraiment adaptées à nos besoins, comme Blender par exemple. Ce logiciel permet de modeliser de très beau objets 3D, mais pas facilement de manière paramétrique, ce qui est ce que nous cherchons

> Travailler de manière paramétrique nous permet de facilement, dés que nécéssaire, changer certaines dimensions afin de s'adapter à de nouvelles contrainte, ou simplement de changer d'échelle.

Il existe également des logiciels propriétaires tel AutoCAD, fusion360, SolidWorks, ... Comme toujours, il est mieux d'essayer de ne pas s'accrocher à ces programmes. Bien que certains de ceux-ci soient accessible gratuitement aux étudiants, une fois ce statut perdu, le travail accomplis sur les serveurs de ces logiciels le sera également.

Finalement, il y a les programmes open-sources. Nous allons nous concentrer sur deux d'entre eux, OpenSCAD et FreeCAD, qui permettent d'obtenir des résultats relativement similaires, mais par des moyens fort différents.

### OpenSCAD

OpenSCAD permet de décrire la géométrie de nos pièces en code python. On utilise des blocs basiques et décrit leur position, orientation et les interactions entre ces blocs.
Les blocs de bases sont comme on peut s'y attendre des cubes, des cylindre, des spheres, ...
Les opérations entre les blocs peuvent être très simples, union ou difference, ou plus complexes, hull, minkowski, ...

Il existe bien entendu une liste de toutes les fonctions existantes, elle se trouve [sur cette page web](openscad.org/cheatsheet).
En plus de ces fonctions, on peut bien entendu ajouter des librairies supplémentaires, comme par exemple la librairie bosl, qui ajoute énormément de formes et transformations préfaites.

A noter qu'il est également facilement possible de trouver des pièces faites par d'autres personnes en ligne sur des plateformes de partage, comme Thingiverse. A noter que Thingiverse est en train de changer son modèle économique et est à mes yeux de moins en moins bien. Je commence à déplacer mes habitudes vers PrusaPrinters.

### FreeCAD

FreeCAD est plus comme SolidWorks avec plein de modules et un système de click and drop des objets. C'est de la modélisation par contrainte, cela signifie qu'il faudra essayer de contraindre tous les points afin de prévenir tous mouvements.

Il existe un module OpenSCAD dans FreeCAD qui permet de facilement ajouter certains détails qui seraient trop complexes à ajouter uniquement via OpenSCAD.



## Différentes définitions

* Structure compliante : une structure sans parties mobile, mais qui peut malgré tout avoir des mouvements grâce à de la flexion ou de la torsion. Limite l'usure.
* Mecanisme bistable : object ayant deux positions d'équilibres. Si suffisament de force est appliqué dans un état, on change d'état.
* Materiaux auxetique : matériaux dont la géométrie fait qu'une force appliquée dans une direction entraine un mouvement dans un autre.

## La licence

On va travailler avec du code et des objets 3D. Il est important d'assigner une licence à nos créations afin de permettre à tout un chacun de réutiliser notre travail, tout en pouvant conditionner cette réutilisation.
Nous allons utiliser les licences Creative Commons qui permettent de facilement et de manière uniformisée de partager des créations au public et d'autoriser celui-ci à utiliser les créations sous les lois du copyright.

Il y a quatre options à définir dans notre licence :
> * BY : il faut créditer le créateur
> * SA : Toute adaptation doit être partager sous la même licence
> * NC : L'utilisation doit être non commerciale
> * ND : Aucun dérivé ou adaptation ne peut être réalisée

J'ai personnelement choisi pour ce code de le placer sous la licence CC BY-NC-CA.

En plus de l'information du type de licence, il est important de partager le nom de l'auteur, le nom du projet ainsi que la date de création.

```
// File : flexlink.scad
//
// Author : Benjamin Hainaut
//
// Date : 16 mars 2022
//
// License : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
```

## Flexlinks

Notre objectif de la semaine va être de reproduire le système de connexion compliante flexlink. Celui-ci permet de joindre des briques LEGO Technique entre elle avec des liens flexibles, certains pouvant être bistable.

J'ai décidé de faire cela dans OpenSCAD, simplement parce que l'interface est plus simple à prendre en main et que le programme est amplement suffisant pour cette tâche.

Le premier défi est de connaitre les dimensions des pièces LEGO sur lesquelles nos pièce iront. J'ai pour ce faire simplement été voir sur Thingiverse un projet déjà existant, et j'ai trouvé que les trous ont un diamètre de 4 millimètres.

Ensuite j'ai commencé à coder. J'ai défini quelques variables :
* Taille des trous
* Taille des pièces
* Nombre de trous
* Distance entre les deux pièces
* Distance entre les trous

J'ai défini une fonction qui fait les deux blocs troué et leur position en fonction des paramètres, et ensuite une petite fonction qui calcule l'angle pour le lien entre les blocs et le place.

aaaaaaaaaaaaaaaaaaaaaaa j'ai pas sauvegarder mon code et mon PC est mort, j'ai tout perdu, il faut recommencer

## Flexlinks 2.0

Les objectifs restent les mêmes, mais profitant de mon expérience, j'en ai profité pour essayer de faire quelque chose d'encore plus modulaire.

J'ai commencé à nouveau par définir quelques variables
```
$fn = 100;

hri = 2;//Hole Radius In
hro = 4;//Hole Radius Out
hd = 8;//Hole distance
height = 10;

gs = 10;//Grid Size
```
On apperçoit la variable gs qui va définir la taille de la grille dans laquelle on va travailler. Ici elle est définie à 10, on a donc une grille de 10x10. Chaque "Case" de la grille est un possible trou de connexion avec les LEGOs.

Maintenant que j'ai toutes mes variables, j'ai défini deux modules. Le premier crée les blocs de connexion, le deuxième les lien entre ceux-ci.

```
module brick(list = [1]){
    difference(){
        hull(){
            for(i=[0:1:gs*gs-1]){
                if(len(search(i, list,0)) >= 1){
                    translate([i%gs*hd,floor(i/gs)*hd,0])cylinder(h=height,r=hro,center=true);
                }
            }
        }
        for(i=[0:1:gs*gs-1]){
                if(len(search(i, list,0)) >= 1){
                    translate([i%gs*hd,floor(i/gs)*hd,0])cylinder(h=height,r=hri,center=true);
                }
            }
    }
}
```

```
module link(A=0,B=35){
    length = sqrt(((B-A)%gs*hd-hro)^2+(floor((B-A)/gs)*hd-hro)^2);
    angle = atan(floor((B-A)/gs)/((B-A)%gs));
    translate([A%gs*hd+hri,floor(A/gs)*hd-0.5+hri,-5])rotate([0,0,angle])cube([length,1,height]);
}
```

Le module brick est tout simplement une coque autour de gros cylindres percés par de plus petits.

Le module link nécéssite quand à lui de calculer la longueur entre les deux points qu'on veut lier, ainsi que l'angle qu'il y aura entre notre lien et l'axe x.

Ensuite, l'utilisation de ce petit script est simple, on choisi les points auquels on veut un bloc, et les points qu'on veut lier. Ensuite on les encode sous forme de bricks et de links.

![ ](../images/module02/GRID2.PNG)

Par example, l'union suivante nous permet d'obtenir un chouette résultat

```
union(){
    brick([0,11]);
    brick([99,88]);
    brick([36,45,54,63]);
    brick([36,37,38]);
    brick([63,73,83]);
    link(11,88);
}
```

![ ](../images/module02/flexlink_example.PNG)
